// To parse this JSON data, do
//
//     final postShare = postShareFromJson(jsonString);

import 'dart:convert';

String postShareToJson(PostShare data) => json.encode(data.toJson());

class PostShare {
  PostShare({
    this.phoneNumber,
    required this.message,
    required this.type,
  });

  String? phoneNumber;
  String message;
  String type;

  factory PostShare.fromJson(Map<String, dynamic> json) => PostShare(
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        message: json["message"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "message": message,
        "type": type,
      };
}
